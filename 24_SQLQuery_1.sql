SELECT P.Name, P.ProductNumber, PS.Name AS ProductSubCategoryName
FROM Production.Product P -- P = Table Alias
INNER JOIN Production.ProductSubcategory PS -- PS = Table Alias
ON P.ProductSubcategoryID = PS.ProductSubcategoryID
-- 295 rows

SELECT *
FROM Production.Product
-- 504 rows

-- Ran BookNormalizationDDL.sql

SELECT *
FROM Book


SELECT B.Title, B.ISBN, B.PublishDate, P.PublisherName
FROM Book B
INNER JOIN Publisher P
ON B.PublisherID = P.PublisherID

SELECT *
FROM Publisher

-- INNER JOIN only returns results if row exists in both tables
SELECT P.FirstName, P.LastName, E.EmailAddress
FROM Person.Person P
INNER JOIN Person.EmailAddress E
ON E.BusinessEntityID = P.BusinessEntityID

SELECT P.FirstName, P.LastName, E.EmailAddress, PP.PhoneNumber
FROM Person.Person P
INNER JOIN Person.EmailAddress E
ON E.BusinessEntityID = P.BusinessEntityID
INNER JOIN Person.PersonPhone PP
ON PP.BusinessEntityID = P.BusinessEntityID

-- Synatax
SELECT A.[Column1], A.[Column2], B.[Column3]
FROM [Table A] A 
LEFT OUTER JOIN [TABLE] B 
ON A.[Primary Key] = B.[Foreign Key]
-- This will return all rows from Table A, then Column 3 value from Table B
-- where the primary key value from Table A matches the foreign key's value in Table B